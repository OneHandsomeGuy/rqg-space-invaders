﻿public class MainMenuUIView : UIView
{
    /// <summary>
    /// Starts game
    /// </summary>
    public void StartGame()
    {
        _ = AppStateController.Instance.SetState(new GameState());
    }

    /// <summary>
    /// Goes to high scores
    /// </summary>
    public void ShowHighScores()
    {
        _ = AppStateController.Instance.SetState(new MenuHighScoresState());
    }
}
