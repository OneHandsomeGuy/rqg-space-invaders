﻿using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonUIView : ViewMVC
{
    #region data fields and properties
    [SerializeField] private TextMeshProUGUI buttonTMP;
    #endregion

    /// <summary>
    /// Setups UI element
    /// </summary>
    /// <param name="buttonText">Passed text for button</param>
    /// <param name="onButtonClickAction">Passed action for button</param>
    public void Setup(string buttonText, UnityAction onButtonClickAction)
    {
        if (buttonText != null && buttonTMP != null)
            buttonTMP.SetText(buttonText);
        if (onButtonClickAction != null)
            GetComponent<Button>().onClick.AddListener(onButtonClickAction);
    }
}
