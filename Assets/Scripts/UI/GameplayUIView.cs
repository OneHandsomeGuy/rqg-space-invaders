﻿using TMPro;
using UnityEngine;

public class GameplayUIView : UIView
{
    #region data fields and properties
    [SerializeField] private TextMeshProUGUI currentWaveTMP, currentScoreTMP, livesLeftTMP;
    #endregion

    /// <summary>
    /// Refreshes UI with current data
    /// </summary>
    internal override void RefreshUI()
    {
        base.RefreshUI();
        SetCurrentWave(NPCController.Instance.CurrentWave);
        SetCurrentScore(ScoreController.Instance.CurrentScore);
        SetCurrentLives(PlayerController.Instance.CurrentLifes);
    }

    /// <summary>
    /// Sets wave text field
    /// </summary>
    /// <param name="wave"></param>
    private void SetCurrentWave(int wave)
    {
        currentWaveTMP?.SetText($"CURRENT WAVE: {wave}");
    }

    /// <summary>
    /// Sets score text field
    /// </summary>
    /// <param name="score"></param>
    private void SetCurrentScore(int score)
    {
        currentScoreTMP?.SetText($"CURRENT SCORE: {score}");
    }

    /// <summary>
    /// Sets lives text field
    /// </summary>
    /// <param name="lifesLeft"></param>
    private void SetCurrentLives(int lifesLeft)
    {
        livesLeftTMP?.SetText($"LIFES LEFT: {lifesLeft}");
    }

    /// <summary>
    /// Returns to main menu
    /// </summary>
    public void BackToMenu()
    {
        _ = AppStateController.Instance.SetState(new MenuState());
    }

}
