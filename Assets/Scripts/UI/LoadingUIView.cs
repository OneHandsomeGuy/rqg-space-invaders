﻿using TMPro;
using UnityEngine;

public class LoadingUIView : UIView
{
    #region data fields and properties
    [SerializeField] private string defaultLoadingText = "Loading...";
    [SerializeField] private TextMeshProUGUI progressText, loadingText;
    #endregion

    /// <summary>
    /// Set progress to be shown on UI - values 0 to 100 with additional message
    /// </summary>
    /// <param name="progress">progress value</param>
    /// <param name="loadingMessage">additional message</param>
    internal void SetProgress(float progress, string loadingMessage = null)
    {
        if (progressText != null)
        {
            progress = Mathf.Clamp(progress, 0, 100);
            progressText?.SetText($"{progress.ToString("n0")}%");
        }
        if (loadingText != null)
        {
            loadingText.SetText(loadingMessage != null ? loadingMessage : defaultLoadingText);
        }
    }
}
