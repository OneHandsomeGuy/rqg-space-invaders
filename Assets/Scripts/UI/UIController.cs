﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIController : ControllerMVC<UIController>
{
    #region data fields and properties
    [SerializeField] private List<UIView> uiViewPrefabs;
    [SerializeField] private Transform uiViewsParent;

    private List<UIView> uiViewInstances;
    private LoadingUIView loadingScreen = null;
    #endregion

    /// <summary>
    /// Sets selected UI visibility, instantiates if not present
    /// </summary>
    /// <param name="UIType">What UI</param>
    /// <param name="show">Should be visible?</param>
    public void SetUIVisibility(UIType UIType, bool show = true)
    {
        if (uiViewInstances == null)
            uiViewInstances = new List<UIView>();
        UIView foundUI = uiViewInstances.Find(match => match.UIType == UIType);
        if (foundUI == null && uiViewPrefabs != null)
        {
            UIView toAdd = uiViewPrefabs.Find(match => match.UIType == UIType);
            if (toAdd != null)
            {
                foundUI = Instantiate(toAdd, uiViewsParent);
                uiViewInstances.Add(foundUI);
            }
            else
            {
                Debug.LogWarning($"Cannot find prefab for {UIType.ToString()}!");
            }
        }
        if (foundUI != null)
        {
            foundUI.SetUIVisibility(show);
        }
    }

    /// <summary>
    /// Refreshes currently visible UI's
    /// </summary>
    public void RefreshCurrentUIs()
    {
        if (uiViewInstances != null)
        {
            uiViewInstances.ForEach(instance => instance.RefreshUI());
        }
    }

    /// <summary>
    /// Hides all available UIs
    /// </summary>
    internal void HideAllUIInstances()
    {
        if (uiViewInstances != null)
            uiViewInstances.ForEach(view => view?.SetUIVisibility(false));
    }

    /// <summary>
    /// Sets loading progress
    /// </summary>
    /// <param name="percentage">Progress percentage</param>
    internal void SetLoadingProgress(float percentage, string loadingMessage = null)
    {
        if (loadingScreen == null)
            loadingScreen = uiViewInstances.Find(match => match.UIType == UIType.LoadingScreen) as LoadingUIView;
        if (loadingScreen != null)
            loadingScreen.SetProgress(percentage, loadingMessage);
    }
}