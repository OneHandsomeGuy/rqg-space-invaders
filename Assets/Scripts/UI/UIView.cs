﻿using UnityEngine;

public class UIView : ViewMVC
{
    #region data fields and properties
    [SerializeField] internal UIType UIType;
    #endregion

    /// <summary>
    /// Sets UI visible or invisible
    /// </summary>
    /// <param name="show"></param>
    internal void SetUIVisibility(bool show)
    {
        gameObject.SetActive(show);
        RefreshUI();
    }

    /// <summary>
    /// Refreshes current UI, if visible
    /// </summary>
    internal virtual void RefreshUI()
    {
        if (!gameObject.activeInHierarchy)
            return;
    }
}
