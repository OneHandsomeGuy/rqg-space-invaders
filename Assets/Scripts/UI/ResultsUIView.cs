﻿using TMPro;
using UnityEngine;

public class ResultsUIView : UIView
{
    #region data fields and properties
    [SerializeField] private TextMeshProUGUI currentWaveTMP, currentScoreTMP;
    #endregion

    /// <summary>
    /// Refreshes UI with current data
    /// </summary>
    internal override void RefreshUI()
    {
        base.RefreshUI();
        SetCurrentWave(NPCController.Instance.CurrentWave);
        SetCurrentScore(ScoreController.Instance.LastScore);
    }

    /// <summary>
    /// Sets wave text field
    /// </summary>
    /// <param name="wave"></param>
    public void SetCurrentWave(int wave)
    {
        currentWaveTMP?.SetText($"SURVIVED WAVES: {wave}");
    }

    /// <summary>
    /// Sets score text field
    /// </summary>
    /// <param name="score"></param>
    public void SetCurrentScore(int wave)
    {
        currentScoreTMP?.SetText($"THIS GAME SCORE: {wave}");
    }

    /// <summary>
    /// Returns to main menu
    /// </summary>
    public void BackToMenu()
    {
        _=AppStateController.Instance.SetState(new MenuState());
    }
}
