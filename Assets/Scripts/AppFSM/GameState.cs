﻿using System.Threading.Tasks;

public class GameState : IAppState
{
    public Task<IAppState> DoState()
    {
        UIController.Instance.HideAllUIInstances();
        PlayerController.Instance.Setup();
        NPCController.Instance.Setup();
        TouchController.Instance.SetTouchActive(true, 1);
        UIController.Instance.SetUIVisibility(UIType.Gameplay);
        return null;
    }
}
