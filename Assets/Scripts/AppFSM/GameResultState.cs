﻿using System.Threading.Tasks;

public class GameResultState : IAppState
{
    public Task<IAppState> DoState()
    {
        if (TouchController.Instance != null)
            TouchController.Instance.SetTouchActive(false);
        PlayerController.Instance.Deactivate();
        UIController.Instance.HideAllUIInstances();
        UIController.Instance.SetUIVisibility(UIType.Results);
        ScoreController.Instance.SaveScore();
        ScoreController.Instance.ResetScore();
        return null;
    }
}
