﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class LoadingState : IAppState
{
    public async Task<IAppState> DoState()
    {
        if (TouchController.Instance != null)
            TouchController.Instance.SetTouchActive(false);
        UIController.Instance.SetUIVisibility(UIType.LoadingScreen);
        await AssetController.Instance.Setup();
        await LoadingController.Instance.LoadSceneAsync(SceneName.MainGame);
        return new MenuState();
    }
}
