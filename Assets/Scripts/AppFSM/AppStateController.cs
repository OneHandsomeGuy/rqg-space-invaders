﻿using System.Threading.Tasks;

public class AppStateController : ControllerMVC<AppStateController>
{
    #region data fields and properties
    private IAppState currentState;
    #endregion

    /// <summary>
    /// Loads main scene
    /// </summary>
    private async Task Start()
    {
        await SetState(new LoadingState());
    }

    /// <summary>
    /// Set app state
    /// </summary>
    /// <param name="passedState">What app state?</param>
    internal async Task SetState(IAppState passedState)
    {
        currentState = passedState;
        var nextState = await passedState.DoState();
        if (nextState != null)
        {
            await SetState(nextState);
        }
    }

    /// <summary>
    /// Returns true if currently in gameplay
    /// </summary>
    /// <returns>true if currently in gameplay</returns>
    internal bool IsGamePlaying()
    {
        return currentState as GameState != null;
    }
}