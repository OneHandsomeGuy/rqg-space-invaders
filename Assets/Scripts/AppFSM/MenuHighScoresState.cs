﻿using System.Threading.Tasks;

public class MenuHighScoresState : IAppState
{
    public Task<IAppState> DoState()
    {
        UIController.Instance.HideAllUIInstances();
        UIController.Instance.SetUIVisibility(UIType.HighScores);
        return null;
    }
}
