﻿using System.Threading.Tasks;

public class MenuState : IAppState
{
    public Task<IAppState> DoState()
    {
        if (TouchController.Instance != null)
            TouchController.Instance.SetTouchActive(false);
        UIController.Instance.HideAllUIInstances();
        UIController.Instance.SetUIVisibility(UIType.MainMenu);
        return null;
    }
}
