﻿using UnityEngine;

[CreateAssetMenu(menuName = "Config/App", fileName = "AppConfig")]
public class AppConfig : ScriptableObject
{
    #region data fields and properties
    [SerializeField, Tooltip("Enemy ships boundary limit will be multiplied by this value (0.6 - 0.95)")]
    private float boundaryMultiplyRatio;

    public float BoundaryMultiplyRatio
    {
        get
        {
            return Mathf.Clamp(boundaryMultiplyRatio, 0.6f, 0.95f);
        }
    }
    #endregion
}