﻿using System;
using UnityEngine;

public class ShipModel : ModelMVC
{
    #region data fields and properties
    public int ShipLifes
    {
        get; private set;
    }
    public int GunThresholdMiliseconds
    {
        get; private set;
    }
    public float ShipEnergy
    {
        get; private set;
    }
    public float ShipMovementSpeed
    {
        get; private set;
    }
    public bool IsFriendly
    {
        get;
        private set;
    } = false;

    private Vector3 targetShipPosition;
    private Action additionalActionAfterPlayerDied;
    private float maxShipEnergy;
    #endregion

    public ShipModel(int shipLifes, float shipEnergy, float shipMovementSpeed, bool isFriendly = false, int gunThresholdMiliseconds = 100)
    {
        ShipLifes = shipLifes;
        ShipEnergy = shipEnergy;
        maxShipEnergy = shipEnergy;
        ShipMovementSpeed = shipMovementSpeed;
        IsFriendly = isFriendly;
        GunThresholdMiliseconds = gunThresholdMiliseconds;
    }

    /// <summary>
    /// Hits player with passed hit points, returns true if player energy was equal/below zero
    /// </summary>
    /// <param name="hitPoints">How many points subtract from ship "life" (0 - inf.)</param>
    public bool HitShip(float hitPoints)
    {
        hitPoints = Mathf.Clamp(hitPoints, 0, Mathf.Infinity);
        ShipEnergy -= hitPoints;
        if (ShipEnergy <= 0)
        {
            ShipEnergy = 0;
            ShipLifes--;
            if (ShipLifes > 0)
            {
                ShipEnergy = maxShipEnergy;
                return true;
            }
            else
            {
                additionalActionAfterPlayerDied?.Invoke();
            }
        }
        return ShipEnergy == 0;
    }

    /// <summary>
    /// Return true if is alive
    /// </summary>
    /// <returns></returns>
    internal bool IsAlive()
    {
        return ShipEnergy > 0;
    }

    /// <summary>
    /// Move desired local ship position at X and Z
    /// </summary>
    /// <param name="movementX">X axis units</param>
    /// <param name="movementZ">Z axis units</param>
    internal void MoveShip(float movementX, float movementZ = 0)
    {
        targetShipPosition.x = Mathf.Clamp(targetShipPosition.x + movementX, BoundaryController.Instance.GetHorizontalBoundary(), -BoundaryController.Instance.GetHorizontalBoundary());
        targetShipPosition.z = targetShipPosition.z + movementZ;
    }

    /// <summary>
    /// Set new target position
    /// </summary>
    /// <param name="newPosition"></param>
    internal void SetTargetPosition(Vector3 newPosition)
    {
        targetShipPosition = newPosition;
    }

    /// <summary>
    /// Assign additional action when died
    /// </summary>
    /// <param name="additionalActionAfterPlayerDied"></param>
    internal void SetAdditionalActionWhenDied(Action additionalActionAfterPlayerDied)
    {
        if (additionalActionAfterPlayerDied != null)
            this.additionalActionAfterPlayerDied = additionalActionAfterPlayerDied;
    }

    /// <summary>
    /// Return target position
    /// </summary>
    /// <returns></returns>
    internal Vector3 GetTargetPosition()
    {
        return targetShipPosition;
    }

    /// <summary>
    /// Reset target position
    /// </summary>
    internal void ResetTargetPosition()
    {
        targetShipPosition = Vector3.zero;
    }
}
