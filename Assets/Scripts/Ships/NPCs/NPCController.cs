﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class NPCController : ControllerMVC<NPCController>
{
    #region data fields and properties
    [Header("Basic config")]
    [SerializeField] private NPCsConfig npcsConfig;
    [SerializeField] private Transform npcSpawnPoint;

    public int CurrentWave
    {
        get;
        private set;
    }

    private List<NPCView> currentNPCViews;
    private float verticalDistance, horizontalDistance;
    private bool directionRight = false;
    #endregion

    /// <summary>
    /// On start, get distances
    /// </summary>
    private void Start()
    {
        verticalDistance = BoundaryController.Instance.GetVerticalBoundary() / 5;
        horizontalDistance = BoundaryController.Instance.GetHorizontalBoundary() / 7;
    }

    /// <summary>
    /// Setup this controller - spawn NPCs if not spawned, set initial values etc.
    /// </summary>
    public void Setup()
    {
        if (currentNPCViews == null)
        {
            currentNPCViews = new List<NPCView>();
            if (npcsConfig != null)
            {
                GameObject foundPrefab = AssetController.Instance?.LoadedViewAssets.Find(match => match.GetComponent<NPCView>() != null);
                if (foundPrefab != null)
                {
                    for (int i = 0; i < npcsConfig.SpawnNPCsAtEveryWave; i++)
                    {
                        NPCView nPCView = Instantiate(foundPrefab, npcSpawnPoint != null ? npcSpawnPoint : transform).GetComponent<NPCView>();
                        ShipModel nPCModel = npcsConfig.ShipModel;
                        nPCView.Setup(nPCModel, null);

                        currentNPCViews.Add(nPCView);
                    }
                }
            }
        }
        CurrentWave = 0;
        GenerateNextWave();
        MoveNPCsToNextPosition();
        ShootAndGenerateWaweIfPossible();
    }

    /// <summary>
    /// Makes random NPC shoot bullet, or if all NPCs died and possible - requests next wave generation
    /// </summary>
    private async void ShootAndGenerateWaweIfPossible()
    {
        await Task.Delay(npcsConfig.ShootingThresholdMiliseconds);
        if (Application.isPlaying && AppStateController.Instance.IsGamePlaying())
        {
            List<NPCView> nPCViews = currentNPCViews.FindAll(match => match.IsAlive());
            if (nPCViews != null && nPCViews.Count > 0)
            {
                nPCViews[Random.Range(0, nPCViews.Count)].ShootBullet();
            }
            else
            {
                GenerateNextWave();
            }
            ShootAndGenerateWaweIfPossible();
        }
    }

    /// <summary>
    /// Generates next positions for NPCs
    /// </summary>
    private async void MoveNPCsToNextPosition()
    {
        await Task.Delay(1000);
        if (currentNPCViews != null && Application.isPlaying && AppStateController.Instance.IsGamePlaying())
        {
            float addVerticalPos = 0;
            if (currentNPCViews.Find(match => (BoundaryController.Instance.IsOutOfHorizontalBoundary(match.ShipModel.GetTargetPosition())) && match.IsAlive()) != null)
            {
                directionRight = !directionRight;
                addVerticalPos = verticalDistance;
                if (currentNPCViews.Find(match => match.transform.position.z < -10) != null)
                   _= AppStateController.Instance.SetState(new GameResultState());
            }
            foreach (NPCView nPCView in currentNPCViews)
            {
                if (nPCView.ShipModel.IsAlive())
                {
                    nPCView.ShipModel.MoveShip((directionRight ? -horizontalDistance : horizontalDistance), -addVerticalPos);
                }
            }
            if (AppStateController.Instance.IsGamePlaying())
            {
                MoveNPCsToNextPosition();
            }
        }
    }

    /// <summary>
    /// Generates next wave of NPCs, using existing views instances
    /// </summary>
    private void GenerateNextWave()
    {
        if (currentNPCViews != null)
        {
            directionRight = false;
            CurrentWave++;
            for (int i = 0; i < currentNPCViews.Count; i++)
            {
                if (currentNPCViews[i] != null)
                {
                    currentNPCViews[i].Setup(npcsConfig.ShipModel, null);
                    currentNPCViews[i].ShuffleModel();
                    currentNPCViews[i].ShipModel.SetTargetPosition(new Vector3(GetPositionForColumn(i % (int)(Mathf.Pow(currentNPCViews.Count, 0.6f))),
                        0,
                        GetPositionForRow(i / (int)(Mathf.Pow(currentNPCViews.Count, 0.6f)))));
                }
            }
            UIController.Instance.RefreshCurrentUIs();
        }
    }

    /// <summary>
    /// Moves all npc views to target position
    /// </summary>
    private void Update()
    {
        if (currentNPCViews != null)
            currentNPCViews.ForEach(npc =>
            {
                if (npc != null && npc.ShipModel.IsAlive())
                    npc.MoveToTargetPosition();
            });
    }

    /// <summary>
    /// Returns target world position for passed row
    /// </summary>
    /// <param name="row">Which row</param>
    /// <returns>target world position for passed row</returns>
    private float GetPositionForRow(int row)
    {
        return (BoundaryController.Instance.GetVerticalBoundary() - npcSpawnPoint.transform.position.z) - row * verticalDistance;
    }

    /// <summary>
    /// Returns target world position for passed column
    /// </summary>
    /// <param name="column">Which column</param>
    /// <returns>target world position for passed column</returns>
    private float GetPositionForColumn(int column)
    {
        return (BoundaryController.Instance.GetHorizontalBoundary() - npcSpawnPoint.transform.position.x) - column * horizontalDistance;
    }
}
