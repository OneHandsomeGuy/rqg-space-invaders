﻿using DG.Tweening;
using UnityEngine;

public class BulletView : ViewMVC
{
    #region data fields and properties
    [SerializeField] private Rigidbody bulletRigidbody;
    [SerializeField] private BoxCollider boxCollider;

    private BulletState bulletState = BulletState.Used;
    private bool friendly = true;
    #endregion

    /// <summary>
    /// On awake, get components if not setten
    /// </summary>
    private void Awake()
    {
        if (bulletRigidbody == null)
            bulletRigidbody = GetComponent<Rigidbody>();
        if (boxCollider == null)
            boxCollider = GetComponent<BoxCollider>();
    }

    /// <summary>
    ///// Returns true if bullet can be reused
    ///// </summary>
    ///// <returns></returns>
    //internal bool CanBeReused()
    //{
    //    return bulletState == BulletState.Free;
    //}

    internal void Setup(Transform fromTransform, bool friendly)
    {
        this.friendly = friendly;
        gameObject.layer = LayerMask.NameToLayer(friendly ? BulletController.Instance.PlayerBulletLayerName : BulletController.Instance.EnemyBulletLayerName);
        bulletState = BulletState.Used;
        if (fromTransform != null)
        {
            transform.position = fromTransform.position;
            transform.rotation = fromTransform.rotation;
        }
        if (bulletRigidbody != null && transform != null)
            bulletRigidbody?.AddForce(transform.forward * 20, ForceMode.Impulse);
        transform.DOScale(1, 1);
    }

    /// <summary>
    /// Reuse view if possible
    /// </summary>
    /// <param name="fromTransform">From which transform should be "shot"</param>
    /// <param name="friendly">Is bullet friendly?</param>
    /// <returns>True if can be reused</returns>
    internal bool Reuse(Transform fromTransform, bool friendly)
    {
        if (bulletState == BulletState.Used)
            return false;
        else
        {
            if (boxCollider != null)
                boxCollider.enabled = true;
            Setup(fromTransform, friendly);
            return true;
        }
    }

    /// <summary>
    /// Perform actions of "destroying" bullet when it hits target
    /// </summary>
    private void DestroyBullet()
    {
        bulletState = BulletState.Free;
        if (bulletRigidbody != null)
        {
            bulletRigidbody.velocity = Vector3.zero;
            bulletRigidbody.angularVelocity = Vector3.zero;
        }
        if (boxCollider != null)
            boxCollider.enabled = false;
        transform.DOScale(0, 1);
    }

    /// <summary>
    /// On collision enter destroy bullet and add damage when hitten target
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        ShipView touchedShip = collision.transform.GetComponent<ShipView>();
        if (touchedShip != null)
        {
            touchedShip.HitShip(10);
        }
        DestroyBullet();
    }
}