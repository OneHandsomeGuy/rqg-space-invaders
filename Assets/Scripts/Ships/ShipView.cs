﻿using DG.Tweening;
using System;
using System.Threading.Tasks;
using UnityEngine;

public class ShipView : ViewMVC
{
    #region data fields and properties
    [SerializeField] private BulletView bulletViewPrefab;
    [SerializeField] private SphereCollider sphereCollider;

    public ShipModel ShipModel
    {
        get;
        private set;
    }
    private bool gunLocked = false;
    private bool invulnerable = false;
    #endregion

    /// <summary>
    /// Get collider on awake
    /// </summary>
    private void Awake()
    {
        if (sphereCollider == null)
            sphereCollider = GetComponent<SphereCollider>();
    }

    /// <summary>
    /// Setups element with passed data
    /// </summary>
    /// <param name="model">Ship model</param>
    /// <param name="additionalActionAfterPlayerDied">additional action</param>
    public void Setup(ShipModel model, Action additionalActionAfterPlayerDied)
    {
        ShipModel = model;
        ShipModel.ResetTargetPosition();
        ResetLocalPosition();
        SetState(ShipViewState.Fine);
        ShipModel.SetAdditionalActionWhenDied(additionalActionAfterPlayerDied);
    }

    /// <summary>
    /// Sets view's state
    /// </summary>
    /// <param name="shipViewState">state - destroyed or fine</param>
    public void SetState(ShipViewState shipViewState)
    {
        if (shipViewState == ShipViewState.Destroyed)
        {
            transform.DOScale(0, 0.9f);
            if (sphereCollider != null)
                sphereCollider.enabled = false;
        }
        else
        {
            transform.DOScale(1, 1f);
            if (sphereCollider != null)
                sphereCollider.enabled = true;
        }
    }

    /// <summary>
    /// moves desired local position
    /// </summary>
    /// <param name="pos">Move by X units</param>
    internal void MoveShip(float pos)
    {
        if (ShipModel != null)
            ShipModel.MoveShip(pos);
    }

    /// <summary>
    /// Smoothly translate view to target position
    /// </summary>
    internal void MoveToTargetPosition()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, ShipModel.GetTargetPosition(), Time.deltaTime * ShipModel.ShipMovementSpeed);
    }

    /// <summary>
    /// Immediately reset local view position
    /// </summary>
    internal void ResetLocalPosition()
    {
        transform.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Shoot bullet
    /// </summary>
    public void ShootBullet()
    {
        if (gunLocked || !ShipModel.IsAlive())
            return;
        if (transform != null && ShipModel != null)
            BulletController.Instance.ShootBullet(transform, ShipModel.IsFriendly);
        gunLocked = true;
        UnlockGun();
    }

    /// <summary>
    /// Unlocks gun
    /// </summary>
    private async void UnlockGun()
    {
        await Task.Delay(ShipModel.GunThresholdMiliseconds);
        gunLocked = false;
    }

    /// <summary>
    /// Hit this ship, check if it "died", if yes than take life (if possible)
    /// </summary>
    /// <param name="hitpoints"></param>
    public void HitShip(float hitpoints)
    {
        if (!invulnerable && ShipModel != null)
        {
            hitpoints = Mathf.Clamp(hitpoints, 0, Mathf.Infinity);
            if (ShipModel.HitShip(hitpoints))
            {
                if (ShipModel.ShipLifes > 0)
                {
                    MakeInvulnerable(3);
                    ResetLocalPosition();
                }
                else
                {
                    SetState(ShipViewState.Destroyed);
                    if (!ShipModel.IsFriendly)
                        ScoreController.Instance.AddToCurrentScore(1);
                }
                UIController.Instance.RefreshCurrentUIs();
            }
        }
    }

    /// <summary>
    /// Makes player invulnerable for passed amount of seconds
    /// </summary>
    /// <param name="seconds">How long player should stay invulnerable (seconds - 1 to 10)</param>
    private async void MakeInvulnerable(int seconds)
    {
        invulnerable = true;
        await Task.Delay(Mathf.Clamp(seconds, 1, 10)*1000);
        invulnerable = false;
    }

    /// <summary>
    /// True if ship is "alive"
    /// </summary>
    /// <returns></returns>
    internal bool IsAlive()
    {
        if (ShipModel != null)
        {
            return ShipModel.IsAlive();
        }
        return false;
    }
}

