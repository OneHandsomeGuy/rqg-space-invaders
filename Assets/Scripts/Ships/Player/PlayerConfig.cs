﻿using UnityEngine;

[CreateAssetMenu(menuName = "Config/Player", fileName = "PlayerConfig")]
public class PlayerConfig : ScriptableObject
{
    #region data fields and properties
    [SerializeField] private int playerLifes, gunThresholdMiliseconds = 100;
    [SerializeField] private float playerEnergy, shipMovementSpeed;

    public ShipModel ShipModel
    {
        get
        {
            ShipModel shipModel = new ShipModel(Mathf.Clamp(playerLifes, 1, 20),
                Mathf.Clamp(playerEnergy, 10, 1000),
                Mathf.Clamp(shipMovementSpeed, 1, 100),
                true,
                Mathf.Clamp(gunThresholdMiliseconds, 20, 2000));
            return shipModel;
        }
    }
    #endregion
}