﻿using UnityEngine;

public class BoundaryController : ControllerMVC<BoundaryController>
{
    #region data fields and properties
    [SerializeField] private AppConfig appConfig;
    [SerializeField] private Camera checkThisCameraBoundary;

    private Vector3 boundPoint;
    private float boundaryMultiplyRatio = 1;
    #endregion

    /// <summary>
    /// At awake should compute boundary from setten or main camera (if not setten)
    /// </summary>
    public void Awake()
    {
        if (checkThisCameraBoundary == null)
            checkThisCameraBoundary = Camera.main;
        if (checkThisCameraBoundary != null)
        {
            if (appConfig != null)
                boundaryMultiplyRatio = appConfig.BoundaryMultiplyRatio;
            boundPoint = checkThisCameraBoundary.ScreenToWorldPoint(new Vector3(0, Screen.height, checkThisCameraBoundary.transform.position.y));
        }
    }

    /// <summary>
    /// Returns vertical boundary - Z
    /// </summary>
    /// <returns>Worldspace vertical boundary</returns>
    public float GetVerticalBoundary()
    {
        return boundPoint != null ? boundPoint.z * boundaryMultiplyRatio : 0;
    }

    /// <summary>
    /// Returns horizontal boundary - X
    /// </summary>
    /// <returns>Worldspace horizontal boundary</returns>
    public float GetHorizontalBoundary()
    {
        return boundPoint != null ? boundPoint.x * boundaryMultiplyRatio : 0;
    }

    /// <summary>
    /// Returns true if passed position (worldspace) is at boundary
    /// </summary>
    /// <param name="position">Passed position</param>
    /// <returns>True if out of X boundary</returns>
    internal bool IsOutOfHorizontalBoundary(Vector3 position)
    {
        bool isOutside = position.x < 0 ? (position.x <= GetHorizontalBoundary()) : (position.x >= -GetHorizontalBoundary());
        return isOutside;
    }
}
