using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AssetController : ControllerMVC<AssetController>
{
    #region data fields and properties
    [SerializeField] private List<AssetReference> viewAssetReferences;
    [SerializeField] private List<AssetReference> meshesAssetReferences;

    public List<GameObject> LoadedViewAssets
    {
        get;
        private set;
    }
    
    public List<GameObject> LoadedMeshAssets
    {
        get;
        private set;
    }

    public bool IsReady
    {
        get;
        private set;
    } = false;
    #endregion

    /// <summary>
    /// Loads all assets
    /// </summary>
    /// <returns>Task</returns>
    public async Task Setup()
    {
        if (LoadedViewAssets == null)
        {
            LoadedViewAssets = await LoadAssets(viewAssetReferences, "Loading views...");
        }
        if (LoadedMeshAssets == null)
        {
            LoadedMeshAssets = await LoadAssets(meshesAssetReferences, "Loading meshes...");
        }
        IsReady = true;
    }

    /// <summary>
    /// Loads passed list of asset references
    /// </summary>
    /// <param name="assetReferences"></param>
    /// <returns>List of loaded GOs</returns>
    public async Task<List<GameObject>> LoadAssets(List<AssetReference> assetReferences, string loadingMessage = null)
    {
        List<GameObject> currentlyLoadedAssets = new List<GameObject>();
        if (assetReferences != null)
        {
            for (int i = 0; i < assetReferences.Count; i++)
            {
                if (assetReferences[i].RuntimeKeyIsValid())
                {
                    AsyncOperationHandle asyncOperationHandle = assetReferences[i].LoadAssetAsync<GameObject>();
                    while (!asyncOperationHandle.IsDone)
                    {
                        await Task.Delay(100);
                        UIController.Instance?.SetLoadingProgress(i / (0.01f * assetReferences.Count), loadingMessage);
                    }
                    if (asyncOperationHandle.Result != null)
                    {
                        currentlyLoadedAssets.Add(asyncOperationHandle.Result as GameObject);
                    }
                }
                else
                {
                    Debug.LogWarning($"Invalid AssetReference key {assetReferences[i].RuntimeKey.ToString()}");
                }
            }
        }
        return currentlyLoadedAssets;
    }

    /// <summary>
    /// Returns random NPC mesh, if available
    /// </summary>
    /// <returns></returns>
    internal GameObject GetRandomNPCMesh()
    {
        if (LoadedMeshAssets != null && LoadedMeshAssets.Count > 0)
        {
            return LoadedMeshAssets[Random.Range(0, LoadedMeshAssets.Count)];
        }
        return null;
    }
}
