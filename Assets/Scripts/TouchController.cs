﻿using System;
using System.Threading.Tasks;
using UnityEngine;

public class TouchController : ControllerMVC<TouchController>
{
    #region data fields and properties
    [SerializeField] private float touchSensitivity = 0.1f;

    private enum TouchState
    {
        NONE,
        TOUCH,
    }

    private Action OnTouchBegin, OnTouchEnd;
    private Action<float> OnTouchHold;
    private TouchState touchState = TouchState.NONE;
    private Vector2? previousTouch, currentTouch;
    public float TouchDiff
    {
        get;
        private set;
    }

    public bool TouchActive
    {
        get; private set;
    }

    private void Start()
    {
        TouchActive = false;
    }
    #endregion

    /// <summary>
    /// On update, check if there are any touches (additionally, for testing - mouse buttons pressed)
    /// </summary>
    private void Update()
    {
        if (TouchActive)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                if (touchState == TouchState.NONE && Input.touchCount >= 1)
                {
                    TouchBegin(Input.touches[0].position);
                }
                else if (touchState == TouchState.TOUCH && Input.touchCount >= 1)
                {
                    TouchHold(Input.touches[0].position);
                }
                else if (touchState == TouchState.TOUCH && Input.touchCount < 1)
                {
                    TouchEnd();
                }
            }
            else if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                if (touchState == TouchState.NONE && Input.GetMouseButtonDown(0))
                {
                    TouchBegin(Input.mousePosition);
                }
                else if (touchState == TouchState.TOUCH && Input.GetMouseButton(0))
                {
                    TouchHold(Input.mousePosition);
                }
                else if (touchState == TouchState.TOUCH && Input.GetMouseButtonUp(0))
                {
                    TouchEnd();
                }
            }
        }
    }

    /// <summary>
    /// Perform when user begun touching
    /// </summary>
    /// <param name="position"></param>
    private void TouchBegin(Vector2 position)
    {
        previousTouch = position;
        touchState = TouchState.TOUCH;
        OnTouchBegin?.Invoke();
    }

    /// <summary>
    /// Perform when user is touching
    /// </summary>
    /// <param name="position">Touch position</param>
    private void TouchHold(Vector2 position)
    {
        if (position != null)
            currentTouch = position;
        ComputeTouchDifference();
        previousTouch = currentTouch;
        OnTouchHold?.Invoke(TouchDiff * touchSensitivity);
    }

    /// <summary>
    /// Perform when user released touch
    /// </summary>
    private void TouchEnd()
    {
        touchState = TouchState.NONE;
        OnTouchEnd?.Invoke();
    }

    /// <summary>
    /// Adds action on touch begin
    /// </summary>
    /// <param name="action">Action to add</param>
    public void AddOnTouchBeginAction(Action action)
    {
        OnTouchBegin += action;
    }

    /// <summary>
    /// Adds action on touch hold
    /// </summary>
    /// <param name="action">Action to add</param>
    public void AddOnTouchHoldAction(Action<float> action)
    {
        OnTouchHold += action;
    }

    /// <summary>
    /// Adds action on touch end
    /// </summary>
    /// <param name="action">Action to add</param>
    public void AddOnTouchEndAction(Action action)
    {
        OnTouchEnd += action;
    }

    /// <summary>
    /// Computes touch difference between previous and current
    /// </summary>
    private void ComputeTouchDifference()
    {
        if (previousTouch != null && currentTouch != null && previousTouch.HasValue && currentTouch.HasValue)
        {
            TouchDiff = currentTouch.Value.x - previousTouch.Value.x;
        }
    }

    /// <summary>
    /// Activates or deactivates touch
    /// </summary>
    /// <param name="touchActive">Should touch be active?</param>
    /// <param name="delaySeconds">Delay of activation/deactivation</param>
    public async void SetTouchActive(bool touchActive = true, int delaySeconds = 0)
    {
        previousTouch = null;
        if (delaySeconds > 0)
            await Task.Delay(1000 * delaySeconds);
        TouchActive = touchActive;
    }
}
