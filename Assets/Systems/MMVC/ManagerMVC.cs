﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class ManagerMVC<T> where T : new()
{
    public static T Instance
    {

        get
        {
            if (m_Instance == null)
            {
                m_Instance = new T();
            }
            return m_Instance;
        }
    }
    private static T m_Instance;

    #region readiness of manager
    public bool Ready
    {
        get
        {
            return m_Ready;
        }
        internal set
        {
            m_Ready = value;
        }
    }
    private bool m_Ready = false;
    #endregion

    public List<ModelMVC> models;

    public virtual void Initialize()
    {
        models = new List<ModelMVC>();
    }

    public virtual async Task InitializeAsync()
    {
        models = new List<ModelMVC>();
        await Task.Delay(1);
    }

    internal async Task GetModels(Task<List<ModelMVC>> task)
    {
        models = await task;
        Ready = true;
    }

    internal void AddModels(List<ModelMVC> newModels)
    {
        if (newModels != null)
        {
            if (models == null)
            {
                models = new List<ModelMVC>();
            }
            foreach (ModelMVC newModel in newModels)
            {
                models.Add(newModel);
            }
        }
    }

    internal async Task WaitUntillIsReady()
    {
        while (!Ready)
            await Task.Delay(300);
    }
}