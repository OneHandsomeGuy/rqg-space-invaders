﻿using System.Threading.Tasks;
using UnityEngine;

public class ControllerMVC<T> : MonoBehaviour where T : MonoBehaviour
{
    public static T Instance
    {
        get
        {

            if (m_Instance == null)
                m_Instance = (T)FindObjectOfType(typeof(T));
            return m_Instance;
        }
        private set
        {
        }
    }
    private static T m_Instance;

    //Use it to initialize controller like creating model, setting up some values etc.
    public virtual void Initialize() { }

    //Use it to enable controller
    public virtual void Enable() { }

    //Use it to disable controller when you want to reuse it in future
    public virtual void Disable() { }
}